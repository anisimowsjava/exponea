<?php

declare(strict_types=1);

namespace Provider;


use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Symfony\Component\Yaml\Yaml;


/**
 * Service provider for using YAML configuration files
 */
class YamlParametersServiceProvider implements ServiceProviderInterface
{

    /**
     * @var string
     */
    protected $file;

    /**
     * @param $file
     */
    public function __construct($file)
    {
        $this->file = $file;
    }

    /**
     * @param Container $app
     */
    public function register(Container $app)
    {

        $config = Yaml::parse(file_get_contents($this->file));

        if (is_array($config)) {

            $this->importSearch($config, $app);

            if (isset($app['config']) && is_array($app['config'])) {

                $app['config'] = array_replace_recursive($app['config'], $config);

            } else {

                $app['config'] = $config;

            }

        }

    }

    /**
     * Looks for import directives
     *
     * @param array $config
     */
    protected function importSearch(&$config, $app) {

        foreach ($config as $key => $value) {

            if ($key === 'imports') {

                foreach ($value as $resource) {

                    $base_dir = str_replace(basename($this->file), '', $this->file);

                    $new_config = new YamlParametersServiceProvider($base_dir . $resource['resource']);

                    $new_config->register($app);

                }

                unset($config['imports']);

            }

        }

    }

}