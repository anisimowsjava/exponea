<?php

declare(strict_types=1);

/**
 * @var $application Ivoba\Silex\Console\Application
 */
$application = $app['console'];

$application->add(new Exponea\Command\NotificationConsumerCommand());

return $application;