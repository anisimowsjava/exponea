<?php

declare(strict_types=1);

namespace Exponea\Service;


use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use Psr\Log\LoggerInterface;

class  NotificationService implements ConsumerInterface
{

    const ANDROID_OS = 'android';

    const EXPNONEA_REQUEST_URL = 'http://api.exponea.com/crm/customers';

    /**
     * @var ClientInterface
     */
    private $client;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var array
     */
    private $config;

    /**
     * PushService constructor.
     * @param ClientInterface $client
     * @param LoggerInterface $logger
     * @param array $config
     */
    public function __construct(ClientInterface $client, LoggerInterface $logger, array $config)
    {

        $this->client = $client;

        $this->logger = $logger;

        $this->config = $config;

    }


    /**
     * Отправка данных в exponea
     * @inheritdoc
     */
    public function execute(AMQPMessage $msg)
    {

        if ($params = $this->prepareExponeaParams($msg)) {

            try {
                //$this->logger->info('SendRequest: ' . $msg->getBody());
                $response = $this->client->request(
                    'POST',
                    self::EXPNONEA_REQUEST_URL,
                    ['json' => $params]
                );

            } catch (GuzzleException $e) {

                $this->logger->error('Exponea request error: ' . $e->getMessage());

            }

        }

        if (!empty($response)) {

            if (
                !($body = $response->getBody())
                || !($body = $body->getContents())
                || !($body = json_decode($body, true))
                || empty($body['success'])
            ) {

                $error_desc = !empty($body['error']) && is_array($body['error']) ? implode(', ', $body['error']) : null;

                $this->logger->error('' . $error_desc . $body);

            }

        }

    }

    private function prepareExponeaParams(AMQPMessage $msg) : array
    {

        $result = array();

        $msgData = json_decode($msg->getBody(), true);

        if (JSON_ERROR_NONE !== json_last_error()) {

                $this->logger->error('Receive wrong message: ' . $msg->getBody() . ' ; json_decode error: ' . json_last_error_msg());

        }

        if (
            !empty($msgData['token'])
            && !empty($msgData['uid'])
            && !empty($msgData['os'])
            && strtolower($msgData['os']) === self::ANDROID_OS
        ) {

            $result = [
                'ids' => ['registered' => $msgData['uid']],
                'project_id' => $this->config['project_id'],
                'properties' => ['google_push_notification_id' => $msgData['token']]
            ];

        }

        return $result;

    }

}