<?php

declare(strict_types=1);

namespace Exponea\Command;


use Ivoba\Silex\Command\Command;
use OldSound\RabbitMqBundle\RabbitMq\Consumer;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class NotificationConsumerCommand extends Command
{

    protected function configure()
    {
        $this->setName('rabbitmq:exponea:notification');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $app = $this->getSilexApplication();

        /**
         * @var Consumer $consumer
         */
        $consumer = $app['rabbit.consumer']['exponea_notification'];

        //запускаем демон
        $consumer->consume(1);
    }

}