<?php

declare(strict_types=1);

use Exponea\Service\NotificationService;
use fiunchinho\Silex\Provider\RabbitServiceProvider;
use Ivoba\Silex\Provider\ConsoleServiceProvider;

require_once __DIR__ . '/../vendor/autoload.php';

$app = new Silex\Application();

$app->register(new Provider\YamlParametersServiceProvider(__DIR__ . '/parameters.yml'));

$app->register(new Silex\Provider\MonologServiceProvider(), array(
    'monolog.logfile' => __DIR__.'/../var/logs/error.log',
));

$app->register(new SilexGuzzle\GuzzleServiceProvider(), array(
    'guzzle.timeout' => 5,
    'allow_redirects' => true,
));

$app['notification_service'] = function ($app) {
    return new NotificationService($app['guzzle'], $app['monolog'], $app['config']['exponea']);
};

$rmq_conf = $app['config']['rmq'];

$app->register(new RabbitServiceProvider(), [
    'rabbit.connections' => [
        'retail_push' => [
            'host'      => $rmq_conf['host'],
            'port'      => $rmq_conf['port'],
            'user'      => $rmq_conf['user'],
            'password'  => $rmq_conf['password'],
            'vhost'     => $rmq_conf['vhost']
        ],
    ],
    'rabbit.consumers' => [
        'exponea_notification' => [
            'connection'        => 'retail_push',
            'exchange_options'  => ['name' => 'register_notification_token','type' => 'fanout'],
            'queue_options'     => ['name' => 'OWOX_Test'],
            'callback'          => 'notification_service'
        ]
    ]
]);

$app->register(new ConsoleServiceProvider(), array(
    'console.name'              => 'RetailApplication',
    'console.version'           => '1.0.0',
    'console.project_directory' => __DIR__
));

return $app;

